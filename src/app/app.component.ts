import { Component } from '@angular/core';
import { ApiService } from './api.service';
import { Chartecters } from './interfaces/chartecters';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'font-end';
  charactesId: Chartecters[];
  charactes: Chartecters[] = [];

  constructor(private _api: ApiService) {
    this._api.getCharactersId()
    .subscribe((res: Chartecters[]) => this.charactesId = res )
    .add(() => this.iterationCharacters());
  }

  iterationCharacters() {
    this.charactesId.forEach((res: Chartecters) => this.viewCharacters(res.id));

  }

  viewCharacters(name: string) {
    this._api.getCharacter(name)
      .subscribe((res: Chartecters) => this.charactes.push(res));
  }


}
