import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  _url: string;
  constructor(private _http: HttpClient) {
    this._url = environment.endpoint;
  }

  public getCharactersId(): Observable<any> {
    return this._http.get(`${this._url}characters/?filter[fields][id]=true`);
  }

  public getCharacter(character: string): Observable<any> {
    return this._http.get(`${this._url}character/${character.toLowerCase()}`);
  }
}
