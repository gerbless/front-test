FROM node:8.11 as node
 MAINTAINER Germain Bueno <germainrafael.buenotaguariparo@externos-cl.cencosud.com>

WORKDIR /app
COPY package.json /app/
RUN npm install
COPY ./ /app/
RUN npm run-script build-prod

FROM nginx:1.13
COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
